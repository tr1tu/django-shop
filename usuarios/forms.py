from django.forms import ModelForm
from usuarios.models import Direccion, User
from django import forms

class DireccionForm(ModelForm):
	class Meta:
		model = Direccion
		exclude = ('usuario',)

class UsuarioForm(ModelForm):
	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email',)