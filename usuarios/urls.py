from django.conf.urls import url, include
from . import views
from django.conf.urls.static import static
from django.conf import settings
from usuarios.views import PerfilUsuarioView, NuevaDireccionView, EditarDireccionView, EditarPerfilView, DarBajaView

urlpatterns = [
	url(r'^registro/$', views.registro, name='registro'),
	url(r'^logout/$', views.cerrar_sesion, name='logout'),
	url(r'^perfil/$', PerfilUsuarioView.as_view(), name='perfil'),
	url(r'^perfil/editar/$', EditarPerfilView.as_view(), name='editarperfil'),
	url(r'^direccion/nueva/$', NuevaDireccionView.as_view(), name='nuevadireccion'),
	url(r'^direccion/editar/(?P<direccion_id>[0-9]+)/$', EditarDireccionView.as_view(), name='editardireccion'),
	url(r'^direccion/eliminar/(?P<direccion_id>[0-9]+)/$', views.eliminar_direccion, name='eliminardireccion'),
	url(r'^darbaja/$', DarBajaView.as_view(), name='darbaja'),
	url('^', include('django.contrib.auth.urls')),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)