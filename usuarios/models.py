from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

# Create your models here.

@python_2_unicode_compatible
class Direccion(models.Model):
	descripcion = models.CharField(max_length=255)
	pais = models.CharField(max_length=70)
	provincia = models.CharField(max_length=50)
	ciudad = models.CharField(max_length=100)
	codigo_postal = models.CharField(max_length=5)
	tel_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="El numero de telefono debe seguir el formato: '+999999999'. Hasta 15 digitos permitidos.")
	telefono = models.CharField(validators=[tel_regex], max_length=15)
	telefono_movil = models.CharField(validators=[tel_regex], max_length=15)
	domicilio = models.CharField(max_length=100)
	usuario = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.descripcion;
