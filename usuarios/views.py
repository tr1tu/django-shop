from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import logout
from django.views.generic import View
from usuarios.forms import DireccionForm, UsuarioForm
from django.urls import reverse
from usuarios.models import Direccion, User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin



# Create your views here.

def registro(request):
	if request.user.id:
		return redirect(reverse('home:index'))
	if request.method == "POST":
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect(reverse('usuarios:perfil'))
	else:
		form = UserCreationForm()
	return render(request, 'registration/register.html', { 'form' : form })

@login_required
def cerrar_sesion(request):
	logout(request)
	return redirect(reverse('home:index'))

class PerfilUsuarioView(LoginRequiredMixin, View):
	template_name = 'usuario/perfil.html'
	def get(self, request, *args, **kwargs):
		usuario = request.user
		return render(request, self.template_name, { 'usuario' : usuario })

class NuevaDireccionView(LoginRequiredMixin, View):
	form_class = DireccionForm
	template_name = 'usuario/nueva_direccion.html'

	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render(request, self.template_name, { 'form' : form, 'new' : True })

	def post(self, request, *args, **kwargs):
		usuario = get_object_or_404(User, pk=request.user.id)
		form = self.form_class(request.POST)
		if form.is_valid():
			direccion = form.save(commit=False)
			direccion.usuario = usuario
			direccion.save()
			return redirect(reverse('usuarios:perfil'))
		return render(request, self.template_name, { 'form' : form , 'new' : True })

class EditarDireccionView(LoginRequiredMixin, View):
	form_class = DireccionForm
	template_name = 'usuario/nueva_direccion.html'

	def get(self, request, *args, **kwargs):
		direccion_id = kwargs['direccion_id']
		direccion = get_object_or_404(Direccion, pk=direccion_id)
		if direccion.usuario != request.user:
			return redirect(reverse('usuarios:perfil'))
		form = self.form_class(instance = direccion)
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		direccion_id = kwargs['direccion_id']
		direccion = get_object_or_404(Direccion, pk=direccion_id)
		if direccion.usuario != request.user:
			return redirect(reverse('usuarios:perfil'))

		form = self.form_class(request.POST, request.FILES, instance=direccion)

		if form.is_valid():
			form.save()
			return redirect(reverse('usuarios:perfil'))
		return render(request, self.template_name, { 'form' : form })

class EditarPerfilView(LoginRequiredMixin, View):
	form_class = UsuarioForm
	template_name = 'usuario/editar_perfil.html'

	def get(self, request, *args, **kwargs):
		form = self.form_class(instance = request.user)
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST, request.FILES, instance = request.user)

		if form.is_valid():
			form.save()
			return redirect(reverse('usuarios:perfil'))
		return render(request, self.template_name, { 'form' : form })

@login_required
def eliminar_direccion(request, direccion_id):
	direccion = get_object_or_404(Direccion, pk=direccion_id)

	if direccion.usuario != request.user:
		return redirect(reverse('usuarios:perfil'))

	direccion.delete()
	return redirect(reverse('usuarios:perfil'))

class DarBajaView(LoginRequiredMixin, View):
	def get(self, request, *args, **kwargs):
		return redirect(reverse('usuarios:perfil'))
	def post(self, request, *args, **kwargs):
		user = request.user
		user.is_active = False
		user.save()
		logout(request)
		return redirect(reverse('home:index'))

