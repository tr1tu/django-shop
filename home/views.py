from django.shortcuts import render
from compras.forms import BuscarPedido
from productos.models import Producto, Categoria

# Create your views here.

def index(request):
	buscar_pedido_form = BuscarPedido()
	productos = Producto.objects.order_by('?')[:9]
	categorias = Categoria.objects.all()
	return render(request, 'home/index.html', { 'buscarpedidoform' : buscar_pedido_form, 'productos' : productos, 'categorias' : categorias });