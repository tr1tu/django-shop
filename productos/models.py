from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models

# Create your models here.

@python_2_unicode_compatible
class Categoria(models.Model):
	nombre = models.CharField(max_length=150)
	descripcion = models.TextField()
	activa = models.BooleanField(default=True)

	def __str__(self):
		return self.nombre

@python_2_unicode_compatible
class Producto(models.Model):
	nombre = models.CharField(max_length=255)
	precio = models.DecimalField(max_digits=20, decimal_places=2)
	descripcion = models.TextField()
	stock = models.IntegerField()
	imagen = models.FileField(upload_to='imagenes/productos/')
	categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)

	def __str__(self):
		return self.nombre
