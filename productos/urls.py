from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
	url(r'^productos/$', views.lista_productos, name='listaproductos'),
	url(r'^productos/(?P<catid>[0-9]+)/$', views.lista_productos, name='listaproductoscat'),
	url(r'^productos/buscar/(?P<busqueda>.+)/$', views.lista_productos, name='listaproductosbus'),
	url(r'^productos/detalle/(?P<id>[0-9]+)/$', views.detalles_producto, name='detallesproducto'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)