from django.shortcuts import render, get_object_or_404
from productos.models import Producto, Categoria
from random import randint
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
def lista_productos(request, catid = -1):
	categorias = Categoria.objects.all()

	if catid != -1 and Categoria.objects.filter(pk=catid).count():
		productos = Categoria.objects.get(pk=catid).producto_set.all()
	elif request.GET.get('busqueda'):
		productos = Producto.objects.filter(Q(nombre__icontains=request.GET['busqueda']) | Q(descripcion__icontains=request.GET['busqueda'])).all()
	else:
		productos = Producto.objects.all()

	page = request.GET.get('page')
	paginator = Paginator(productos, 24)

	try:
		productos = paginator.page(page)
	except PageNotAnInteger:
		productos = paginator.page(1)
	except EmptyPage:
		productos = paginator.page(paginator.num_pages)


	return render(request, 'productos/productos_lista.html', { 'productos' : productos, 'categorias' : categorias })

def detalles_producto(request, id):
	producto = get_object_or_404(Producto, pk=id)
	count = producto.categoria.producto_set.count()
	index1 = randint(0, count)
	index2 = randint(0, count)
	while index1 == index2:
		index2 = randint(0, count)

	if index1 > index2:
		relacionados = producto.categoria.producto_set.all()[index2:index1][:5]
	else:
		relacionados = producto.categoria.producto_set.all()[index1:index2][:5]

	return render(request, 'productos/productos_detalle.html', { 'producto' : producto, 'relacionados' : relacionados })
