from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings
from compras.forms import RealizarPedido1, RealizarPedido2
from compras.views import RealizarPedidoWizard, ListaPedidosView, DetallePedidoView

urlpatterns = [
	url(r'^carrito/add/(?P<producto_id>[0-9]+)/$', views.add_carrito, name='addcarrito'),
	url(r'^carrito/add/(?P<producto_id>[0-9]+)/(?P<cantidad>[1-9][0-9]*)/$', views.add_carrito, name='addcarritocant'),
	url(r'^carrito/$', views.ver_carrito, name='vercarrito'),
	url(r'^carrito/eliminar/(?P<producto_id>[0-9]+)/$', views.eliminar_producto_carrito, name='eliminarproductocarrito'),
	url(r'^realizarpedido/$', RealizarPedidoWizard.as_view([RealizarPedido1, RealizarPedido2]), name='realizarpedido'),
	url(r'^pedidos/$', ListaPedidosView.as_view(), name='listapedidos'),
	url(r'^pedido/(?P<id>[0-9]+)/$', DetallePedidoView.as_view(), name='detallepedido'),
	url(r'^pedido/confirmar/(?P<id>[0-9]+)/$', views.confirmar_pedido, name='confirmarpedido'),
	url(r'^pedido/cancelar/(?P<id>[0-9]+)/$', views.cancelar_pedido, name='cancelarpedido'),
	url(r'^pedidocompletado/(?P<id>[0-9]+)/$', views.pedido_completado, name='pedidocompletado'),
	url(r'^buscarpedido/$', views.buscar_pedido, name='buscarpedido'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)