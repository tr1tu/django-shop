from django.shortcuts import render, get_object_or_404, redirect, reverse, HttpResponse
from productos.models import Producto
from django.utils import timezone
from formtools.wizard.views import SessionWizardView
from compras.models import Pedido, ProductoPedido, MetodoPago, MetodoEnvio, ProductoPedido
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import View

# Create your views here.

def add_carrito(request, producto_id, cantidad = 1):
	producto = get_object_or_404(Producto, pk=producto_id)

	carrito = request.session.get('carrito', {})
	carrito[producto_id] = cantidad
	request.session['carrito'] = carrito	

	return redirect(reverse('compras:vercarrito'))

def ver_carrito(request):
	carrito = request.session.get('carrito', {})
	productos = []
	total = 0
	for k,v in carrito.items():
		if Producto.objects.filter(pk=k).exists():
			producto = get_object_or_404(Producto, pk=k)
			cantidadinc = int(v)+1
			cantidaddec = int(v)-1 if int(v)-1 > 0 else 1
			productos.append({ 'producto' : producto, 'cantidad' : v, 'preciototal' : producto.precio * int(v), 'cantidadinc' : cantidadinc, 'cantidaddec' : cantidaddec })
			total = total + producto.precio * int(v)

	return render(request, 'compras/carrito.html', { 'carrito' : productos, 'total' : total })

def eliminar_producto_carrito(request, producto_id):
	carrito = request.session.get('carrito', {})
	del carrito[producto_id]
	request.session['carrito'] = carrito
	return redirect(reverse('compras:vercarrito'))

class RealizarPedidoWizard(SessionWizardView):
	def done(self, form_list, **kwargs):
		carrito = self.request.session.get('carrito', {})
		if not carrito:
			return HttpResponse('No hay productos en el carrito')
		pedido = form_list[0].instance
		pedido.metodo_pago = form_list[1].instance.metodo_pago
		pedido.metodo_envio = form_list[1].instance.metodo_envio
		if not self.request.user.is_anonymous:
			pedido.usuario = self.request.user
		else:
			pedido.estado = 'ES'
		pedido.save()

		for k,v in carrito.items():
			if Producto.objects.filter(pk=k).exists():	
				producto_pedido = ProductoPedido()
				producto = get_object_or_404(Producto, pk=k)
				producto_pedido.producto = producto
				producto_pedido.cantidad = int(v)
				producto_pedido.precio = producto.precio
				producto_pedido.pedido = pedido
				producto_pedido.save()

		del self.request.session['carrito']

		if not self.request.user.is_anonymous:
			return redirect(reverse('compras:detallepedido', kwargs={ 'id' : pedido.id }))
		else:
			return redirect(reverse('compras:pedidocompletado', kwargs={ 'id' : pedido.id }))


class ListaPedidosView(LoginRequiredMixin, View):
	template_name = 'compras/lista_pedidos.html'

	def get(self, request, *args, **kwargs):
		if request.user:
			pedidos = request.user.pedido_set.all()
			for p in pedidos:
				p.precio = 0
				for prod in p.productopedido_set.all():
					p.precio = p.precio + prod.precio * prod.cantidad
				p.precio = p.precio + p.metodo_pago.recargo + p.metodo_envio.coste
			return render(request, self.template_name, { 'pedidos' : pedidos })
		return redirect(reverse('home:index'))

class DetallePedidoView(View):
	template_name = 'compras/detalle_pedido.html'

	def get(self, request, *args, **kwargs):
		if request.user:
			pedido = get_object_or_404(Pedido, pk=kwargs['id'])
			if (not pedido.usuario and request.user.is_anonymous) or pedido.usuario == request.user:
				productos = pedido.productopedido_set.all()
				subtotal = 0
				for p in productos:
					p.preciototal = p.producto.precio * p.cantidad
					subtotal = subtotal + p.preciototal
				total = subtotal + pedido.metodo_pago.recargo + pedido.metodo_envio.coste
				return render(self.request, self.template_name, { 'pedido' : pedido, 'productos' : productos, 'subtotal' : subtotal, 'total' : total })
			return redirect(reverse('compras:listapedidos'))
		return redirect(reverse('usuarios:login'))		

@login_required
def confirmar_pedido(request, id):
	if request.user:
		pedido = get_object_or_404(Pedido, pk=id)
		if pedido.usuario == request.user and pedido.estado == 'SC':
			pedido.estado = 'ES'
			pedido.save()
		return redirect(reverse('compras:listapedidos'))
	return redirect(reverse('home:login'))

@login_required
def cancelar_pedido(request, id):
	if request.user:
		pedido = get_object_or_404(Pedido, pk=id)
		if pedido.usuario == request.user and (pedido.estado == 'SC' or pedido.estado == 'ES' or pedido.estado == 'PR'):
			pedido.estado = 'CA'
			pedido.save()
		return redirect(reverse('compras:listapedidos'))
	return redirect(reverse('home:login'))

def pedido_completado(request, id):
	return render(request, 'compras/pedido_completado.html', { 'idpedido' : id })

def buscar_pedido(request):
	if request.POST:
		idpedido = request.POST.get('idpedido')
		pedido = get_object_or_404(Pedido, pk=idpedido)
		if not pedido.usuario or request.user == pedido.usuario:
			return redirect(reverse('compras:detallepedido', kwargs={ 'id' : idpedido }))
		else:
			return redirect(reverse('home:index'))