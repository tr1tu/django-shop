from django.contrib import admin

from compras.models import Pedido, ProductoPedido, MetodoPago, MetodoEnvio

# Register your models here.

admin.site.register(Pedido)
admin.site.register(ProductoPedido)
admin.site.register(MetodoPago)
admin.site.register(MetodoEnvio)