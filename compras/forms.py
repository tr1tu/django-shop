from compras.models import Pedido, ProductoPedido, MetodoEnvio, MetodoPago
from django import forms
from django.forms import ModelForm

class RealizarPedido1(ModelForm):
	class Meta:
		model = Pedido
		fields = ('nombre_cliente', 'apellidos_cliente', 'email_cliente', 'pais', 'provincia', 
					'ciudad', 'codigo_postal', 'telefono', 'telefono_movil', 'domicilio',)

class RealizarPedido2(ModelForm):
	class Meta:
		model = Pedido
		fields = ('metodo_pago', 'metodo_envio',)

class BuscarPedido(forms.Form):
	idpedido = forms.IntegerField(label = 'Identificador del pedido')