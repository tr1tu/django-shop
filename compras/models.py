from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models

from usuarios.models import User
from productos.models import Producto

from django.utils import timezone

# Create your models here.

@python_2_unicode_compatible
class MetodoPago(models.Model):
	nombre = models.CharField(max_length=100)
	recargo = models.DecimalField(max_digits=20, decimal_places=2)

	def __str__(self):
		return self.nombre + ", +" + str(self.recargo) + " euros"

@python_2_unicode_compatible
class MetodoEnvio(models.Model):
	nombre = models.CharField(max_length=100)
	coste = models.DecimalField(max_digits=20, decimal_places=2)

	def __str__(self):
		return self.nombre + ", +" + str(self.coste) + " euros"

@python_2_unicode_compatible
class Pedido(models.Model):
	usuario = models.ForeignKey(User, on_delete=models.CASCADE, null = True)
	nombre_cliente = models.CharField(max_length=200)
	apellidos_cliente = models.CharField(max_length=200)
	email_cliente = models.CharField(max_length=255)
	pais = models.CharField(max_length=70)
	provincia = models.CharField(max_length=50)
	ciudad = models.CharField(max_length=100)
	codigo_postal = models.CharField(max_length=5)
	telefono = models.CharField(max_length=15)
	telefono_movil = models.CharField(max_length=15)
	domicilio = models.CharField(max_length=100)

	SIN_CONFIRMAR = 'SC'
	EN_ESPERA = 'ES'
	EN_PREPARACION = 'PR'
	ENVIADO = 'EN'
	CANCELADO = 'CA'

	ESTADOS_PEDIDO = (
			(SIN_CONFIRMAR, 'Sin confirmar'),
			(EN_ESPERA, 'En espera'),
			(EN_PREPARACION, 'En preparacion'),
			(ENVIADO, 'Enviado'),
			(CANCELADO, 'Cancelado'),
		)

	estado = models.CharField(max_length=2, choices=ESTADOS_PEDIDO, default=SIN_CONFIRMAR)
	metodo_pago = models.ForeignKey(MetodoPago, on_delete=models.CASCADE)
	metodo_envio = models.ForeignKey(MetodoEnvio, on_delete=models.CASCADE)
	fecha = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.nombre_cliente + " - " + str(self.id)

@python_2_unicode_compatible
class ProductoPedido(models.Model):
	pedido = models.ForeignKey(Pedido, on_delete=models.CASCADE)
	producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
	cantidad = models.IntegerField()
	precio = models.DecimalField(max_digits=20, decimal_places=2)

	def __str__(self):
		return self.pedido.nombre_cliente + " - " + str(self.pedido.id) + " - " + self.producto.nombre